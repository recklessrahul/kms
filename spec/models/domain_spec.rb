# == Schema Information
#
# Table name: domains
#
#  id          :integer          not null, primary key
#  domain_name :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

require 'spec_helper'

describe Domain do

    let(:domain) { Domain.new :domain_name => "RAG" }
    let(:root_domain) { Domain.create_folder(domain) }

    it "should allow creating domains" do
      Domain.list_domains.should include root_domain
    end

    context "Folders" do
      let(:folder) { Domain.new :domain_name => "RAG_CHILD" }
      let(:parent) { root_domain }
      let(:folder_created) { Domain.create_folder(folder, parent) }
      it "should allow creating sub-folders to a domain" do
        folder_created.parent.should == parent
      end

      it "should allow listing sub-directories of a folder" do
        folders = Domain.list_subfolders_of parent
        folders.should include folder_created
      end
    end
  end
