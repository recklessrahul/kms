# == Schema Information
#
# Table name: articles
#
#  id          :integer          not null, primary key
#  subject     :string(255)
#  description :text
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  aasm_state  :string(255)
#  user_id     :integer
#  domain_id   :integer
#


require 'spec_helper'

describe Article do
    let(:user) {stub(:name => "Rahul", :id => 1)}
    let(:article) {Article.new :subject => "test", :user_id => user.id}

    it "should be a draft when saved" do
      article.draft?.should be_true
    end

    it "should not be published when just created" do
      article.published?.should be_false
    end

    it "should raise an error when trying to publish a draft without approval" do 
      expect {article.publish}.to raise_error
    end

    context "Sending for approval" do
      before do
        article.send_for_approval
      end

      it "should not be a draft & should be pending approval" do
        article.approval_pending?.should be_true
      end

      context "Reject an article sent for approval" do
        before do
          article.reject
        end

        it "should be a draft again when rejected" do
          article.rejected?.should be_true
        end
        
        it "should allow refactoring rejected articles by converting them to drafts" do
          article.refactor
          article.draft?.should be_true
        end
      end
    end

    context "Publishing" do
      before do
        article.send_for_approval
        article.publish
      end

      it "should not be a draft & should be pending approval" do
        article.published?.should be_true
      end
    end

    it "should reflect the author's name" do
      article.send_for_approval
      article.publish
      article.should_receive :user
      article.published_by?
    end
end
