$(document).ready(function(){
  $(".editor").wysihtml5({
    "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
    "emphasis": true, //Italics, bold, etc. Default true
    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
    "html": false, //Button which allows you to edit the generated HTML. Default false
    "link": true, //Button to insert a link. Default true
    "image": true, //Button to insert an image. Default true,
    "color": true //Button to change color of font  
  });

  $(".tags").chosen();
  $(".domain").chosen();

  window.nestedFormEvents.insertFields = function(content, assoc, link) {
    console.log(assoc);
    console.log(link);
    if(assoc =='article_attachments') {
      return $(link).closest('form').find(".attachments").prepend($(content));
    }
    if(assoc =='skills') {
      return $(link).closest('form').find(".skills").prepend($(content));
    }
    if(assoc =='interests') {
      return $(link).closest('form').find(".interests").prepend($(content));
    }
  };

  //This is for articles index in the users show action
  $('.feed').html("<i class='icon-spinner icon-4x icon-spin center offset5' style='align: center;'></i>");
  var articles_path = $('.feed').data('articles_path');
  $.get(articles_path, null, null, 'script');

  $(document).on('click','.article_pagination .pagination ul li a', function () {
    $('.feed').html("<i class='icon-spinner icon-4x icon-spin center offset5' style='align: center;'></i>");
    $.get(this.href, null, null, 'script');
    return false;
  });

  $(document).on('click','.ajax_page', function () {
    $('.feed').html("<i class='icon-spinner icon-4x icon-spin center offset5' style='align: center;'></i>");
    // return false;
  });
  $(document).on('click','.loader', function () {
    $('.article_folders').html("<i class='icon-spinner icon-4x icon-spin center offset5' style='align: center;'></i>");
    // return false;
  });

});
