// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery-migrate-1.2.1
//= require jquery_ujs
//= require twitter/bootstrap
//= require bootstrap-wysihtml5
//= require bootstrap-datepicker
//= require chosen-jquery
//= require jquery_nested_form
//= require dataTables/jquery.dataTables
//= require dataTables/jquery.dataTables.bootstrap
//= require jquery.ticker
//= require jquery.placeholder
//= require_tree .

$(document).ready(function(){
  $('#datatables').dataTable({
    "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
    "sPaginationType": "bootstrap",
    "aaSorting": [[ 5, "desc" ]]
  });

  var fadeInImage = function(){
    $(".image_auto_fade").fadeTo('slow',1,function() {
      fadeOutImage();
    });
  };

  var fadeOutImage = function(){
    $(".image_auto_fade").fadeTo('slow',0.8,function() {
      fadeInImage();
    });
  };
  fadeOutImage();

  $('#notifications_feed').ticker({
    htmlFeed: true,
    titleText: 'Notifications',
    direction: 'ltr',       // Ticker direction - current options are 'ltr' or 'rtl'
    pauseOnItems: 6000,    // The pause on a news item before being replaced
    fadeInSpeed: 1000,      // Speed of fade in animation
    fadeOutSpeed: 800,
    controls: false,
    displayType: 'fade'
  });

  $("input, textarea").placeholder();

  $(".dateofbirth").datepicker({
    autoclose: true,
    format: "dd/mm/yyyy",
    startView: 2
  });
});
