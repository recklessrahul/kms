class DomainsController < ApplicationController
  load_and_authorize_resource
  # GET /domains
  # GET /domains.json
  def index
    @folders = Domain.list_domains
    @domains = Domain.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @domains }
      format.js { render :list_folders }
    end
  end

  # GET /domains/1
  # GET /domains/1.json
  def show
    @domain = Domain.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @domain }
    end
  end

  # GET /domains/new
  # GET /domains/new.json
  def new
    @domain = Domain.new
    @parent = @domain.parent
    @folders = Domain.list_domains

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @domain }
    end
  end

  # GET /domains/1/edit
  def edit
    @domain = Domain.find(params[:id])
    @parent = @domain.parent
    @folders = Domain.list_subfolders_of @parent
  end

  # POST /domains
  # POST /domains.json
  def create
    domain = Domain.new(params[:domain])
    if params[:domain_id].present?
      parent = Domain.find params[:domain_id]
    end
    @domain = Domain.create_folder(domain, parent)

    respond_to do |format|
      if @domain.new_record?
        format.html { 
          @folders = Domain.list_subfolders_of parent
          render :new
        }
      else
        format.html { redirect_to domains_path, notice: 'Domain was successfully created.' }
      end
    end
  end

  # PUT /domains/1
  # PUT /domains/1.json
  def update
    @domain = Domain.find(params[:id])
    if params[:domain_id].present?
      parent = Domain.find params[:domain_id]
      @domain.parent = parent
    end

    respond_to do |format|
      if @domain.update_attributes(params[:domain])
        format.html { redirect_to @domain, notice: 'Domain was successfully updated.' }
        format.json { head :no_content }
      else
        @folders = Domain.list_subfolders_of parent
        format.html { render action: "edit" }
        format.json { render json: @domain.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /domains/1
  # DELETE /domains/1.json
  def destroy
    @domain = Domain.find(params[:id])
    @domain.destroy

    respond_to do |format|
      format.html { redirect_to domains_url }
      format.json { head :no_content }
    end
  end

  def list_folders
    @parent = Domain.find params[:id]
    @folders = Domain.list_subfolders_of @parent

    respond_to do |format|
      format.js
    end
  end
end
