class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :require_login
  before_filter :render_notifications
  rescue_from CanCan::AccessDenied, with: :not_authorized

  protected
  def not_authenticated
    redirect_to login_path, :alert => "You're not authorized to access this page. Please login first."
  end

  private
  def not_authorized
    if current_user
      redirect_to user_path(current_user), alert: "Not authorized"
    else
      redirect_to root_path, alert: "Not authorized"
    end
  end

  def render_notifications
    @notifications = Notification.active.last_updated if current_user
  end
end
