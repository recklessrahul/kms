class ArticlesController < ApplicationController
  load_and_authorize_resource

  def index
    @articles = Article.filter_for_feed(params, current_user)
    @feed_header = Article.feed_header(params).try(:html_safe)

    if params[:domain_id].present?
      @domain = Domain.find params[:domain_id] if params[:domain_id].present?
    end
    @domains = Domain.list_subfolders_of @domain

    respond_to do |format|
      format.html # index.html.erb
      format.js
    end
  end

  def new
    @article = Article.new
    @folders = Domain.list_domains
  end

  def edit
    @article = Article.find params[:id]
    @domain = @article.domain
    @parent = @domain
    @folders = Domain.list_subfolders_of @domain
    render :new
  end

  def create
    #Set article's domain_id using the file selector built using js
    params[:article][:domain_id] = params[:domain_id]

    @article = current_user.articles.new params[:article]

    #As the article ID is not saved, attachment does not have
    #access to user, where the upload limit is stored
    if @article.article_attachments.any?
      @article.article_attachments.each do |attachment|
        attachment.user = current_user
      end
    end

    if @article.save
      redirect_to edit_article_path(@article), notice: "Your draft has been saved"
    else
      @domain = @article.domain
      @parent = @domain
      @folders = Domain.list_subfolders_of @domain
      @article.article_attachments.each do |a|
        a.file_name = File.basename a.file.path
      end
      render :new
    end
  end

  def update
    params[:article][:domain_id] = params[:domain_id]

    @article = Article.find params[:id]
    @article.assign_attributes params[:article]

    #As the article ID is not saved, attachment does not have
    #access to user, where the upload limit is stored
    if @article.article_attachments.any?
      @article.article_attachments.each do |attachment|
        attachment.user = current_user
      end
    end


    if @article.save params[:article]
      redirect_to edit_article_path(@article), notice: "The article was updated"
    else
      @domain = @article.domain
      @parent = @domain
      @folders = Domain.list_subfolders_of @domain
      @article.article_attachments.each do |a|
        a.file_name = File.basename a.file.path
      end
      render :new
    end
  end

  def show
    @article = Article.find params[:id]
  end

  def publish
    @article = Article.find params[:id]
    @article.update_attribute(:approver_id, current_user.id)
    @article.publish!
    redirect_to article_path(@article), notice: "This article has been published"
  end

  def reject
    @article = Article.find params[:id]
    begin 
      @article.rejection_reason = params[:rejection_reason]
      if @article.save
        @article.reject!
        @article.update_attribute(:approver_id, current_user.id)
        redirect_to article_path(@article), notice: "This article has been sent back to the author"
      else
        redirect_to article_path(@article), alert: "The reason provided is either too short or too long."
      end
    rescue AASM::InvalidTransition
      redirect_to article_path(@article), alert: "Please provide a valid reason for sending this article back to the author"
    end
  end

  def refactor
    @article = current_user.articles.find params[:id]
    @article.refactor!
    redirect_to article_path(@article), notice: "You can now edit this article"
  end

  def send_for_approval
    @article = current_user.articles.find params[:id]
    @article.send_for_approval!
    redirect_to article_path(@article), notice: "Your article has been sent for approval"
  end

  def unpublish
    @article = current_user.articles.find params[:id]
    @article.unpublish!
    redirect_to article_path(@article), alert: "Your article has been unpublished"
  end

  def destroy
    @article = Article.find params[:id]
    @article.destroy
    redirect_to user_path(current_user), alert: "The article was deleted"
  end

  def like
    @article = Article.find params[:id]
    @article.liked_by current_user
    redirect_to article_path(@article), notice: "You liked this article"
  end

end
