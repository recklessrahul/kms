module UsersHelper

  def render_avatar user, size
    return image_tag(user.avatar.url(:nail).to_s, class: "img-circle") if size == "nail"
    image_tag(user.avatar.url(size.to_sym).to_s, class: "img-polaroid")
  end

  def stats user
    draft = content_tag('div') do
      content_tag('h4', "Drafts") +
      content_tag('h1', 
        link_to(user.draft_articles.count, articles_path(state: 'draft'), remote: true, class: "ajax_page"))
    end

    pending = content_tag('div') do
      content_tag('h4', "Pending") +
      content_tag('h1', 
        link_to(user.approval_pending.count, articles_path(state: 'approval_pending'), remote: true, class: "ajax_page"))
    end

    published = content_tag('div') do
      content_tag('h4', "Published") +
      content_tag('h1', 
        link_to(user.published_articles.count, articles_path(state: 'published'), remote: true, class: "ajax_page"))
    end

    rejected = content_tag('div') do
      content_tag('h4', "Re-Work") +
      content_tag('h1', 
        link_to(user.rejected_articles.count, articles_path(state: 'rejected'), remote: true, class: "ajax_page"))
    end

    return draft + "<hr>".html_safe + pending + "<hr>".html_safe  + published + "<hr>".html_safe + rejected + "<hr>".html_safe 
  end

end
