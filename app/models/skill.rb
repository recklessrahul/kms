# == Schema Information
#
# Table name: skills
#
#  id         :integer          not null, primary key
#  skill_name :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class Skill < ActiveRecord::Base
  attr_accessible :skill_name, :user_id

  belongs_to :user
end
