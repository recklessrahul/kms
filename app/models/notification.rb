class Notification < ActiveRecord::Base
  attr_accessible :content, :subject, :active
  validates :subject, :length => { :maximum => 50 }
  validates :content, :length => { :maximum => 200 }
  scope :last_updated, order('updated_at desc')
  scope :active, where(active: true)
end
