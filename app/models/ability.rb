class Ability
  include CanCan::Ability

  def initialize(user)
    alias_action :read, :update, :destroy, :to => :rud
    case user.role.to_sym
    when :admin
      can :manage, :all
      can :like, Article, published?: true
      cannot :like, Article, user_id: user.id
      can :manage, Tag
      can :manage, Domain
      can :manage, User
      cannot :destroy, User

      can :download, ArticleAttachment
      can :manage, Notification
    when :contributer, :contributor, :domain_head
      can :create, Article

      can :read, Article, published?: true
      can :read, Article, user_id: user.id

      can :update, Article, draft?: true

      can :destroy, Article, draft?: true
      can :destroy, Article, rejected?: true

      can :send_for_approval, Article, user_id: user.id

      can :refactor, Article, user_id: user.id

      can :like, Article, published?: true
      cannot :like, Article, user_id: user.id

      can :read, User
      can :vcard, User
      can :update, User, id: user.id
      can :index, User

      can :download, ArticleAttachment, article: {user_id: user.id}
      can :download, ArticleAttachment, article: {published?: true}

      can :list_folders, Domain
      can :index, Domain
      can :index, Notification

    when :coordinator
      can :create, Article
      # User permissions
      can :read, Article, published?: true
      can :read, Article, user_id: user.id

      can :update, Article, draft?: true

      can :destroy, Article, draft?: true
      can :destroy, Article, rejected?: true

      can :send_for_approval, Article, user_id: user.id

      can :refactor, Article, user_id: user.id
      
      #co-ordinator specific permissions
      can :rud, Article, domain: {parent_domain: user.domain}, approval_pending?: true
      can :publish, Article, domain: {parent_domain: user.domain}
      can :reject, Article, domain: {parent_domain: user.domain}

      can :like, Article, published?: true
      cannot :like, Article, user_id: user.id
      can :read, User
      can :vcard, User
      can :update, User, id: user.id
      can :index, User

      can :download, ArticleAttachment, article: {user_id: user.id}
      can :download, ArticleAttachment, article: {published?: true}
      can :download, ArticleAttachment, article: {domain: {parent_domain: user.domain}}

      can :list_folders, Domain
      can :index, Domain
      can :index, Notification
    end
  end
end
