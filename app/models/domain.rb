# == Schema Information
#
# Table name: domains
#
#  id          :integer          not null, primary key
#  domain_name :string(255)
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Domain < ActiveRecord::Base
  attr_accessible :domain_name, :welcome_message, :ancestry, :banner, :display_image_only, :remove_banner
  has_many :articles
  has_ancestry

  validates_presence_of :domain_name

  validate :uniqueness_of_root_domains
  mount_uploader :banner, BannerUploader

  def self.list_domains
    roots.order('domain_name ASC')
  end

  def self.create_folder domain, parent = nil
    domain.parent = parent if parent
    domain.save
    domain
  end

  def self.list_subfolders_of parent = nil
    return list_domains unless parent
    children_of parent
  end

  def path
    path = []
    path += ancestors.map(&:domain_name) if parent
    path
  end

  def parent_domain
    if ancestors.first
      return ancestors.first
    else
      return self
    end
  end


  private
  def uniqueness_of_root_domains
    ##Reject non-root domains
    return unless self.is_root?

    ##Pluck domains
    root_domains = Domain.list_domains.pluck(:domain_name).map(&:downcase)
    match = root_domains.keep_if { |root_domain_name| self.domain_name.downcase == root_domain_name }
    if self.new_record?
      if match.size > 0
        errors.add :domain_name, "This domain name already exists at the root level"
      end
    else
      if match.size > 1
        errors.add :domain_name, "This domain name already exists at the root level"
      end
    end
  end

end
