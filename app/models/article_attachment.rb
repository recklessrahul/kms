# == Schema Information
#
# Table name: article_attachments
#
#  id         :integer          not null, primary key
#  article_id :integer
#  file       :string(255)
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  file_name  :string(255)
#
require 'file_size_validator' 
class ArticleAttachment < ActiveRecord::Base
  attr_accessible :article_id, :file, :file_name, :file_size, :content_type
  belongs_to :article, touch: true
  mount_uploader :file, AttachmentUploader
  validate :check_file_size
  before_save :set_filename
  before_save :update_file_attributes
  attr_accessor :user

  def upload_limit
    if user.upload_limit.to_f > 0
      upload_limit = user.upload_limit.to_f
    else
      upload_limit = 10
    end
    return @upload_limit ||= upload_limit
  end

  private

  def check_file_size
    if file.file.size.to_f/(1000*1000) > upload_limit.to_f
      errors.add(:file, "You cannot upload a file greater than #{upload_limit.to_f}MB")
    end
  end

  def update_file_attributes
    if file.present? && file_changed?
      self.content_type = file.file.content_type
      self.file_size = file.file.size
    end
  end

  def set_filename
    if file.path
      self.file_name = File.basename(self.file.path)
    end
  end

end
