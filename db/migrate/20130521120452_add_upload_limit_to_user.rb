class AddUploadLimitToUser < ActiveRecord::Migration
  def change
    add_column :users, :upload_limit, :integer
  end
end
