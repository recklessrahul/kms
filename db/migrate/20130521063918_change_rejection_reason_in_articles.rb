class ChangeRejectionReasonInArticles < ActiveRecord::Migration
  def up
    change_column :articles, :rejection_reason, :text
  end

  def down
    change_column :articles, :rejection_reason, :string
  end
end
