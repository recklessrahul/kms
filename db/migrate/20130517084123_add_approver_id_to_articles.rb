class AddApproverIdToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :approver_id, :integer
  end
end
