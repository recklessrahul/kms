class AddRejectionReasonToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :rejection_reason, :string
  end
end
