class AddFileSizeAndContentTypeToArticleAttachments < ActiveRecord::Migration
  def change
    add_column :article_attachments, :file_size, :string
    add_column :article_attachments, :content_type, :string
  end
end
