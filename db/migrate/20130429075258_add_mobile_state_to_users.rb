class AddMobileStateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :mobile, :integer
    add_column :users, :state, :string
  end
end
