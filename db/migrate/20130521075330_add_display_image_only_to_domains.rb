class AddDisplayImageOnlyToDomains < ActiveRecord::Migration
  def change
    add_column :domains, :display_image_only, :boolean
  end
end
