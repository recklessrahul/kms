class AddRetireToUsers < ActiveRecord::Migration
  def change
    add_column :users, :retire, :boolean
  end
end
