class AddWelcomeMessageToDomains < ActiveRecord::Migration
  def change
    add_column :domains, :welcome_message, :text
  end
end
