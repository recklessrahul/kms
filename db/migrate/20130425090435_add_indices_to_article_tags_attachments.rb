class AddIndicesToArticleTagsAttachments < ActiveRecord::Migration
  def up
    execute "create index articles_subject on articles using gin(to_tsvector('english', subject))"
    execute "create index articles_description on articles using gin(to_tsvector('english', description))"
    execute "create index article_attachments_file_name on article_attachments using gin(to_tsvector('english', file_name))"
    add_index :article_attachments, :id
    add_index :article_tags, :id
    add_index :article_tags, :tag_id
    add_index :article_tags, :article_id
    execute "create index domain_name on domains using gin(to_tsvector('english', domain_name))"
    add_index :domains, :id
    execute "create index tag_name on tags using gin(to_tsvector('english', tag_name))"
    add_index :tags, :id
  end

  def down
    execute "drop index articles_subject"
    execute "drop index articles_description"
    remove_index :article_attachments, :id
    remove_index :article_tags, :id
    remove_index :article_tags, :article_id
    remove_index :article_tags, :tag_id
    execute "drop index domain_name"
    remove_index :domains, :id
    execute "drop index tag_name"
    remove_index :tags, :id
  end
end
