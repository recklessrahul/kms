# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130627054516) do

  create_table "article_attachments", :force => true do |t|
    t.integer  "article_id"
    t.string   "file"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.string   "file_name"
    t.string   "file_size"
    t.string   "content_type"
  end

  add_index "article_attachments", ["id"], :name => "index_article_attachments_on_id"

  create_table "article_tags", :force => true do |t|
    t.integer  "article_id"
    t.integer  "tag_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "article_tags", ["article_id"], :name => "index_article_tags_on_article_id"
  add_index "article_tags", ["id"], :name => "index_article_tags_on_id"
  add_index "article_tags", ["tag_id"], :name => "index_article_tags_on_tag_id"

  create_table "articles", :force => true do |t|
    t.string   "subject"
    t.text     "description"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.string   "aasm_state"
    t.integer  "user_id"
    t.integer  "domain_id"
    t.integer  "cached_votes_total", :default => 0
    t.integer  "cached_votes_score", :default => 0
    t.integer  "cached_votes_up",    :default => 0
    t.integer  "cached_votes_down",  :default => 0
    t.text     "rejection_reason"
    t.integer  "approver_id"
  end

  add_index "articles", ["aasm_state"], :name => "index_articles_on_aasm_state"
  add_index "articles", ["cached_votes_down"], :name => "index_articles_on_cached_votes_down"
  add_index "articles", ["cached_votes_score"], :name => "index_articles_on_cached_votes_score"
  add_index "articles", ["cached_votes_total"], :name => "index_articles_on_cached_votes_total"
  add_index "articles", ["cached_votes_up"], :name => "index_articles_on_cached_votes_up"
  add_index "articles", ["domain_id"], :name => "index_articles_on_domain_id"
  add_index "articles", ["user_id"], :name => "index_articles_on_user_id"

  create_table "domains", :force => true do |t|
    t.string   "domain_name"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.text     "welcome_message"
    t.string   "ancestry"
    t.string   "banner"
    t.boolean  "display_image_only"
  end

  add_index "domains", ["ancestry"], :name => "index_domains_on_ancestry"
  add_index "domains", ["id"], :name => "index_domains_on_id"

  create_table "interests", :force => true do |t|
    t.string   "interest_name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "user_id"
  end

  create_table "notifications", :force => true do |t|
    t.string   "subject"
    t.text     "content"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.boolean  "active"
  end

  create_table "skills", :force => true do |t|
    t.string   "skill_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "user_id"
  end

  create_table "tags", :force => true do |t|
    t.string   "tag_name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "tags", ["id"], :name => "index_tags_on_id"

  create_table "users", :force => true do |t|
    t.string   "username",                      :null => false
    t.string   "email"
    t.string   "crypted_password"
    t.string   "salt"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "role"
    t.string   "domain"
    t.text     "about_me"
    t.string   "designation"
    t.string   "avatar"
    t.integer  "mobile",           :limit => 8
    t.string   "state"
    t.datetime "last_login_at"
    t.datetime "last_logout_at"
    t.datetime "last_activity_at"
    t.boolean  "retire"
    t.integer  "upload_limit"
    t.integer  "domain_id"
    t.date     "date_of_birth"
  end

  add_index "users", ["domain_id"], :name => "index_users_on_domain_id"
  add_index "users", ["last_logout_at", "last_activity_at"], :name => "index_users_on_last_logout_at_and_last_activity_at"

  create_table "votes", :force => true do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], :name => "index_votes_on_votable_id_and_votable_type_and_vote_scope"
  add_index "votes", ["votable_id", "votable_type"], :name => "index_votes_on_votable_id_and_votable_type"
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], :name => "index_votes_on_voter_id_and_voter_type_and_vote_scope"
  add_index "votes", ["voter_id", "voter_type"], :name => "index_votes_on_voter_id_and_voter_type"

end
